/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import java.io.FileInputStream;
import java.util.Arrays;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.Sides;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

public class Printer {

    public static void printDocument(String full_path) {

        try {
            DocFlavor flavor = DocFlavor.SERVICE_FORMATTED.PAGEABLE;
            PrintRequestAttributeSet patts = new HashPrintRequestAttributeSet();
            patts.add(Sides.ONE_SIDED);
            PrintService[] ps = PrintServiceLookup.lookupPrintServices(flavor, patts);
            if (ps.length == 0) {

                JOptionPane.showMessageDialog(null, "No Printers found !");
            }

      
            PrintService myService = null;
            String[] possibilities = new String[ps.length];
            int i = 0;
            for (PrintService printService : ps) {
                possibilities[i] = printService.getName();
                i++;

            }

            Icon errorIcon = UIManager.getIcon("OptionPane.QUESTION_MESSAGE");

            String sel_printerName = (String) JOptionPane.showInputDialog(null,
                    "Select a Printer from list", "Select a Printer",
                    JOptionPane.PLAIN_MESSAGE, errorIcon, possibilities, "");

            System.out.println(sel_printerName);

            for (PrintService printService : ps) {

                if (printService.getName().equals(sel_printerName)) {

                    myService = printService;
                    break;
                }
            }

            if (myService == null) {

                JOptionPane.showMessageDialog(null, "Printer not found !");
                return;
            }

            FileInputStream fis = new FileInputStream(full_path);
            Doc pdfDoc = new SimpleDoc(fis, DocFlavor.INPUT_STREAM.AUTOSENSE, null);
            DocPrintJob printJob = myService.createPrintJob();
            printJob.print(pdfDoc, new HashPrintRequestAttributeSet());
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
