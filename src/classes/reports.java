/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

public class reports {

    static String today = null;
    static String PATH = "C:/IL_Farm/";
    static String logo = "images/xchi.png";

    public static String checkPath(String folder) {

        try {
            String directory_path = PATH.concat(new SimpleDateFormat("yyyy_MM_dd").format(new Date()));
            directory_path = directory_path.concat("/" + folder + "/");

            File directory = new File(directory_path);
            if (!directory.exists()) {
                directory.mkdirs();

            }
            return directory_path;
        } catch (Exception e) {
            return null;
        }

    }

    public static String getToday() {
        return new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date());
    }

    public static String daily_Sales(String date) {

        if (checkPath("Sales_Reports") != null) {

            String savePath = checkPath("Sales_Reports");

            Document document = new Document(PageSize.A4.rotate());
            ResultSet invoice_details = null;
            ResultSet invoice = null;
            ResultSet today_customers = null;

            String idcustomer = null;

            String today = date;//new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            String dayEnd = today + " 23:59:59";

            String ful_path = savePath + date + "_" + "DailySale_" + getToday() + ".pdf";

            try {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(ful_path));
                document.open();

                Font fontH1 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.NORMAL);
                System.out.println("2");
                try {
                    today_customers = ConManager.getData("select customer_idcustomer from cash_rec "
                            + "where cash_rec.date between '" + today + "' and '" + dayEnd + "' group by customer_idcustomer");

                } catch (Exception e) {
                    e.printStackTrace();
                    return null;

                }

                PdfPTable table = new PdfPTable(10);

                table.setWidthPercentage(100);
                table.setSpacingBefore(20f);
                table.setSpacingAfter(10f);

                //Set Column widths
                float[] columnWidths = {1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f, 1f};
                table.setWidths(columnWidths);

                PdfPCell m_cell_1 = new PdfPCell(new Phrase("Customer"));

                PdfPCell m_cell_2 = new PdfPCell(new Phrase("Items"));
                m_cell_2.setColspan(5);
                PdfPCell m_cell_3 = new PdfPCell(new Phrase("Other Charges"));
                PdfPCell m_cell_4 = new PdfPCell(new Phrase("Today Total"));
                PdfPCell m_cell_5 = new PdfPCell(new Phrase("Received Payments"));
                m_cell_5.setColspan(2);

                table.addCell(m_cell_1);
                table.addCell(m_cell_2);
                table.addCell(m_cell_3);
                table.addCell(m_cell_4);
                table.addCell(m_cell_5);

//
                boolean sub_title_added = true;

                while (today_customers.next()) {
                    System.out.println("customer in");
                    idcustomer = today_customers.getString("customer_idcustomer");
                    int rowspan = 0;
                    ResultSet inv_items = null;
                    ResultSet cash_pays = null;

                    boolean isInvoiced_Cus = false;

                    String cus_no = null;
                    String cus_name = null;

                    Double oth_charge = 0.00;
                    Double today_tot = 0.00;

                    try { //cash methods count

                        int item_count = 0;
                        int cash_methods_count = 0;

                        ResultSet rsinv = ConManager.getData("select count('idinvoice_det') as 'count' from invoice join invoice_det on "
                                + "invoice.idinvoice = invoice_det.invoice_idinvoice where date between '"
                                + today + "' and '" + dayEnd + "' and customer_idcustomer =" + idcustomer);

                        if (rsinv.next()) {
                            isInvoiced_Cus = true;
                            item_count = rsinv.getInt("count");
                            System.out.println("item count " + item_count);
                        }

                        ResultSet rs = ConManager.getData("select count('idcash_rec') as 'count' from cash_rec "
                                + "where cash_rec.date between '" + today + "' and '" + dayEnd + "' and customer_idcustomer =" + idcustomer);
                        if (rs.next()) {
                            cash_methods_count = rs.getInt("count");
                            System.out.println(" cash_methods_count_" + cash_methods_count);
                        }

                        rowspan = item_count;

                        if (item_count < cash_methods_count) {
                            rowspan = cash_methods_count;
                        }

                        System.out.println("rowspn_" + rowspan);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;

                    }

                    try {

                        ResultSet invoice_data = ConManager.getData("select * from invoice join customer on invoice.customer_idcustomer = customer.idcustomer where date between '"
                                + today + "' and '" + dayEnd + "' and customer_idcustomer =" + idcustomer);
                        while (invoice_data.next()) {

                            cus_no = invoice_data.getString("cus_no");
                            cus_name = invoice_data.getString("name");
                            oth_charge += invoice_data.getDouble("oth_charge");
                            today_tot += invoice_data.getDouble("invoice_tot");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }

                    try {
                        inv_items = ConManager.getData("select * from invoice join customer on"
                                + " invoice.customer_idcustomer = customer.idcustomer join invoice_det on"
                                + " invoice.idinvoice = invoice_det.invoice_idinvoice where date between '"
                                + today + "' and '" + dayEnd + "' and customer_idcustomer =" + idcustomer);
                        System.out.println("select * from invoice join customer on"
                                + " invoice.customer_idcustomer = customer.idcustomer join invoice_det on"
                                + " invoice.idinvoice = invoice_det.invoice_idinvoice where date between '"
                                + today + "' and '" + dayEnd + "' and customer_idcustomer =" + idcustomer);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }

                    try {
                        cash_pays = ConManager.getData("select * from cash_rec where date between '"
                                + today + "' and '" + dayEnd + "' and customer_idcustomer =" + idcustomer);
                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }

                    PdfPCell sub_cell_1 = new PdfPCell(new Phrase(cus_name + "\n(" + cus_no + ")"));
                    sub_cell_1.setRowspan(rowspan + 1);
                    table.addCell(sub_cell_1);

                    if (sub_title_added) {

                        PdfPCell sub_cell_2 = new PdfPCell(new Phrase("Item Type"));
                        PdfPCell sub_cell_3 = new PdfPCell(new Phrase("Sub Type"));
                        PdfPCell sub_cell_4 = new PdfPCell(new Phrase("Quantity Pcs/Box"));
                        PdfPCell sub_cell_5 = new PdfPCell(new Phrase("Quantity KG"));
                        PdfPCell sub_cell_6 = new PdfPCell(new Phrase("Price per KG/Box"));

                        table.addCell(sub_cell_2);
                        table.addCell(sub_cell_3);
                        table.addCell(sub_cell_4);
                        table.addCell(sub_cell_5);
                        table.addCell(sub_cell_6);
                    } else {
                        PdfPCell sub_cell_2 = new PdfPCell(new Phrase(""));
                        PdfPCell sub_cell_3 = new PdfPCell(new Phrase(""));
                        PdfPCell sub_cell_4 = new PdfPCell(new Phrase(""));
                        PdfPCell sub_cell_5 = new PdfPCell(new Phrase(""));
                        PdfPCell sub_cell_6 = new PdfPCell(new Phrase(""));

                        table.addCell(sub_cell_2);
                        table.addCell(sub_cell_3);
                        table.addCell(sub_cell_4);
                        table.addCell(sub_cell_5);
                        table.addCell(sub_cell_6);
                    }

                    PdfPCell sub_cell_7 = new PdfPCell(new Phrase("" + oth_charge));
                    PdfPCell sub_cell_8 = new PdfPCell(new Phrase("" + today_tot));

                    sub_cell_7.setRowspan(rowspan + 1);
                    sub_cell_8.setRowspan(rowspan + 1);

                    table.addCell(sub_cell_7);
                    table.addCell(sub_cell_8);

                    if (sub_title_added) {
                        PdfPCell sub_cell_9 = new PdfPCell(new Phrase("Method"));
                        PdfPCell sub_cell_10 = new PdfPCell(new Phrase("Amount"));

                        table.addCell(sub_cell_9);
                        table.addCell(sub_cell_10);

                        sub_title_added = false;
                    } else {
                        PdfPCell sub_cell_9 = new PdfPCell(new Phrase(""));
                        PdfPCell sub_cell_10 = new PdfPCell(new Phrase(""));

                        table.addCell(sub_cell_9);
                        table.addCell(sub_cell_10);
                    }

                    boolean check_inv_items = inv_items.next();
                    boolean check_cash_pays = cash_pays.next();

                    //  boolean isfirstTime = false; cash_pays.
                    while (check_inv_items || check_cash_pays) {

//                        if (isfirstTime) {
//                            check_inv_items = inv_items.next();
//                            check_cash_pays = cash_pays.next();
//
//                        }
                        System.out.println("in while");

//                         PdfPCell raw_cell_1 = new PdfPCell(new Phrase("c"));
//                            table.addCell(raw_cell_1);
                        if (inv_items.isAfterLast()) {

                            PdfPCell raw_cell_2 = new PdfPCell(new Phrase(""));
                            PdfPCell raw_cell_3 = new PdfPCell(new Phrase(""));
                            PdfPCell raw_cell_4 = new PdfPCell(new Phrase(""));
                            PdfPCell raw_cell_5 = new PdfPCell(new Phrase(""));
                            PdfPCell raw_cell_6 = new PdfPCell(new Phrase(""));

                            table.addCell(raw_cell_2);
                            table.addCell(raw_cell_3);
                            table.addCell(raw_cell_4);
                            table.addCell(raw_cell_5);
                            table.addCell(raw_cell_6);

                        } else {
                            PdfPCell raw_cell_2 = new PdfPCell(new Phrase(inv_items.getString("chick_type")));
                            PdfPCell raw_cell_3 = new PdfPCell(new Phrase(inv_items.getString("sub_type")));
                            PdfPCell raw_cell_4 = new PdfPCell(new Phrase(inv_items.getString("quantity")));
                            PdfPCell raw_cell_5 = new PdfPCell(new Phrase(inv_items.getString("quantity_kg")));
                            PdfPCell raw_cell_6 = new PdfPCell(new Phrase(inv_items.getString("p_rate")));

                            table.addCell(raw_cell_2);
                            table.addCell(raw_cell_3);
                            table.addCell(raw_cell_4);
                            table.addCell(raw_cell_5);
                            table.addCell(raw_cell_6);

                            check_inv_items = inv_items.next();
                        }

//                        PdfPCell raw_cell_7 = new PdfPCell(new Phrase("c" + oth_charge));
//                        table.addCell(raw_cell_7);
//                        PdfPCell raw_cell_8 = new PdfPCell(new Phrase("" + today_tot));
//                        table.addCell(raw_cell_8);
                        if (cash_pays.isAfterLast()) {
                            PdfPCell raw_cell_9 = new PdfPCell(new Phrase(""));
                            PdfPCell raw_cell_10 = new PdfPCell(new Phrase(""));
                            table.addCell(raw_cell_9);
                            table.addCell(raw_cell_10);
                        } else {
                            PdfPCell raw_cell_9 = new PdfPCell(new Phrase(cash_pays.getString("method")));
                            PdfPCell raw_cell_10 = new PdfPCell(new Phrase(cash_pays.getString("pay_amount")));
                            table.addCell(raw_cell_9);
                            table.addCell(raw_cell_10);

                            check_cash_pays = cash_pays.next();
                        }

                    }

                }

                document.add(table);

                document.close();
                writer.close();

            } catch (Exception e) {
                e.printStackTrace();

            }
            return ful_path;
        }
        return null;
    }

    public static String invoice_create(String invoiceId) {

        if (checkPath("Invoice") != null) {

            String savePath = checkPath("Invoice");

            Document document = new Document();//PageSize.A4.rotate()
            ResultSet invoice_details = null;
            ResultSet invoice = null;
            ResultSet payments = null;
            String invoiceTime = null;
            String invoiceNo = null;
            String idcustomer = null;
            String tot_credit = null;
            String cusNo = null;
            String cus_name = null;
            String oth_charge = null;
            String inoive_tot = null;

            try {
                invoice_details = ConManager.getData("SELECT * FROM invoice "
                        + "JOIN invoice_det ON invoice.idinvoice = invoice_det.invoice_idinvoice "
                        + "WHERE idinvoice=" + invoiceId);

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            try {
                invoice = ConManager.getData("SELECT * FROM invoice join customer "
                        + "on invoice.customer_idcustomer = customer.idcustomer WHERE idinvoice=" + invoiceId);
                invoice.next();
                idcustomer = invoice.getString("idcustomer");
                invoiceTime = invoice.getString("date");
                cusNo = invoice.getString("cus_no");
                cus_name = invoice.getString("name");
                tot_credit = invoice.getString("total_credit");
                inoive_tot = invoice.getString("invoice_tot");
                oth_charge = invoice.getString("oth_charge");

                invoiceNo = invoice.getString("invoice_no");

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            try {
                payments = ConManager.getData("select * from cash_rec where customer_idcustomer=" + idcustomer + " order by date desc limit 5");
            } catch (Exception e) {
                return null;
            }

            String full_path = savePath + invoiceNo + "_invoice_" + getToday() + ".pdf";

            try {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(full_path));
                document.open();

                if (new File(PATH + logo).exists()) {
                    Image image1 = Image.getInstance(PATH + logo);
                    image1.setAbsolutePosition(40f, 690f);
                    image1.scaleAbsolute(100, 100);
                    document.add(image1);
                }

                Paragraph paragraph = new Paragraph("                                         Invoice No: " + invoiceNo);
                paragraph.setSpacingBefore(20f);
                document.add(paragraph);

                Paragraph p2 = new Paragraph("                                         Invoice Date : "
                        + new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(invoice.getTimestamp("date")));
                p2.setSpacingBefore(5f);
                document.add(p2);

                Paragraph cus1 = new Paragraph("                                         Customer No: " + cusNo);
                cus1.setSpacingBefore(5f);
                document.add(cus1);

                Paragraph cus2 = new Paragraph("                                         Customer Name : " + cus_name);
                cus2.setSpacingBefore(5f);
                document.add(cus2);

                Paragraph cus3 = new Paragraph("                                         Customer Total Credit Arrears : LKR " + tot_credit);
                cus3.setSpacingBefore(5f);
                document.add(cus3);

                PdfPTable table = new PdfPTable(5); //  columns.
                table.setWidthPercentage(100); //Width 100%
                table.setSpacingBefore(20f); //Space before table
                table.setSpacingAfter(10f); //Space after table

                //Set Column widths
                float[] columnWidths = {2f, 1f, 1f, 1f, 1f};
                table.setWidths(columnWidths);

                BaseColor black = BaseColor.BLACK;

                PdfPCell cell1 = new PdfPCell(new Paragraph("Item Type"));
                cell1.setBorderColor(black);
                cell1.setPaddingLeft(10);
                cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell2 = new PdfPCell(new Paragraph("Quantity PCS/Box"));
                cell2.setBorderColor(black);
                cell2.setPaddingLeft(10);
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell3 = new PdfPCell(new Paragraph("Quantity KG"));
                cell3.setBorderColor(black);
                cell3.setPaddingLeft(10);
                cell3.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell3.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell4 = new PdfPCell(new Paragraph("Price per KG/Box"));
                cell4.setBorderColor(black);
                cell4.setPaddingLeft(10);
                cell4.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell4.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell4_1 = new PdfPCell(new Paragraph("Sub Total"));
                cell4_1.setBorderColor(black);
                cell4_1.setPaddingLeft(10);
                cell4_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell4_1.setVerticalAlignment(Element.ALIGN_MIDDLE);
//
                table.addCell(cell1);
                table.addCell(cell2);
                table.addCell(cell3);
                table.addCell(cell4);
                table.addCell(cell4_1);

                while (invoice_details.next()) {

                    String itmcode = invoice_details.getString("chick_type") + " (" + invoice_details.getString("sub_type") + ")";
                    String quantity_pcs = invoice_details.getString("quantity");
                    String quantity_kg = invoice_details.getString("quantity_kg");
                    String price = invoice_details.getString("p_rate");
                    String sub_tot = invoice_details.getString("sub_tot");

                    PdfPCell cell5 = new PdfPCell(new Paragraph(itmcode));

                    PdfPCell cell6 = new PdfPCell(new Paragraph(quantity_pcs));

                    PdfPCell cell7 = new PdfPCell(new Paragraph(quantity_kg));

                    PdfPCell cell8 = new PdfPCell(new Paragraph("LKR " + price));

                    PdfPCell cell8_1 = new PdfPCell(new Paragraph("LKR " + sub_tot));

                    table.addCell(cell5);
                    table.addCell(cell6);
                    table.addCell(cell7);
                    table.addCell(cell8);
                    table.addCell(cell8_1);

                }
                Paragraph sub_para1 = new Paragraph("                                                                                                               Other Charge : LKR " + oth_charge);
                sub_para1.setSpacingBefore(5f);

                Paragraph sub_para2 = new Paragraph("                                                                                                                 Invoice Total : LKR " + inoive_tot);
                sub_para2.setSpacingBefore(5f);

                Paragraph sub_para = new Paragraph(cus_name + "'s Payment History");
                sub_para.setSpacingBefore(10f);

                PdfPTable table2 = new PdfPTable(3);
                table2.setWidthPercentage(75);
                table2.setSpacingBefore(10f);
                table2.setSpacingAfter(10f);

                //Set Column widths
                float[] columnWidths2 = {1f, 1f, 1f};
                table2.setWidths(columnWidths2);

                PdfPCell cell11 = new PdfPCell(new Paragraph("Payment Date"));
                cell11.setBorderColor(black);
                cell11.setPaddingLeft(10);
                cell11.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell11.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell9 = new PdfPCell(new Paragraph("Payment Method"));
                cell9.setBorderColor(black);
                cell9.setPaddingLeft(10);
                cell9.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell9.setVerticalAlignment(Element.ALIGN_MIDDLE);

                PdfPCell cell10 = new PdfPCell(new Paragraph("Paid Amount"));
                cell10.setBorderColor(black);
                cell10.setPaddingLeft(10);
                cell10.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell10.setVerticalAlignment(Element.ALIGN_MIDDLE);

                table2.addCell(cell11);
                table2.addCell(cell9);
                table2.addCell(cell10);

                while (payments.next()) {

                    PdfPCell cell_pay1 = new PdfPCell(new Paragraph(new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(payments.getTimestamp("date"))));

                    PdfPCell cell_pay2 = new PdfPCell(new Paragraph(payments.getString("method")));

                    PdfPCell cell_pay3 = new PdfPCell(new Paragraph("LKR " + payments.getString("pay_amount")));

                    table2.addCell(cell_pay1);
                    table2.addCell(cell_pay2);
                    table2.addCell(cell_pay3);
                }

                document.add(table);

                document.add(sub_para1);
                document.add(sub_para2);
                document.add(sub_para);

                document.add(table2);

                Paragraph sub_para3 = new Paragraph("     _______________                                                                           _________________");
                sub_para3.setSpacingBefore(15f);
                document.add(sub_para3);

                Paragraph sub_para4 = new Paragraph("     (IL Farm Signature)                                                                           (Customer Signature)");
                sub_para4.setSpacingBefore(1f);
                document.add(sub_para4);

                document.close();
                writer.close();

                JOptionPane.showMessageDialog(null, "Invoice Generated!");

            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(null, "Something Wrong !");
            }

            return full_path;

        } else {
            return null;
        }

    }

}
