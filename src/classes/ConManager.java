package classes;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class ConManager {

    public static final String dbName = "chicken_run";
    private static final String url = "jdbc:mysql://localhost:3306/" + dbName;
    private static final String driverName = "com.mysql.jdbc.Driver";
    public static final String username = "root";
    public static final String password = "4321";
    public static final String mysql_path = "C:\\Program Files\\MySQL\\MySQL Server 5.6\\bin";
    private static Connection con;

    public static boolean checkDBExists(String dbName) {

        try {
            Class.forName(driverName); //Register JDBC Driver

           
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", username, password); //Open a connection

            ResultSet resultSet = con.getMetaData().getCatalogs();

            while (resultSet.next()) {

                String databaseName = resultSet.getString(1);
                if (databaseName.equals(dbName)) {
                  
                    return true;
                }
            }
            resultSet.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return false;
    }

    public static Connection getConnection() {
        try {
            if (checkDBExists(dbName)) {

                con = null;

                try {
                    Class.forName(driverName);

                    try {
                        con = DriverManager.getConnection(url, username, password);

                    } catch (SQLException ex) {

                        JOptionPane.showMessageDialog(null, "System Is Offline ! Contact the System Administrator(1)");
                        ex.printStackTrace();
                    }
                } catch (ClassNotFoundException ex) {

                    JOptionPane.showMessageDialog(null, "System Is Offline ! Contact the System Administrator(2)");
                    ex.printStackTrace();
                }
                return con;
            } else {

                con.createStatement().executeUpdate("create database " + dbName);

                return getConnection();
            }

        } catch (SQLException ex) {

            JOptionPane.showMessageDialog(null, "System Is Offline ! Contact the System Administrator(3)");
            ex.printStackTrace();
            return con;
        }
    }

    public static void putData(String sql) throws Exception {

        if (con == null) {
            getConnection();
        }
        con.createStatement().executeUpdate(sql);

    }

    public static ResultSet getData(String sql) throws Exception {

        if (con == null) {
            getConnection();
        }

        ResultSet rs = con.createStatement().executeQuery(sql);
        return rs;

    }

}
