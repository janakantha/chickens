/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classes;

import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 *
 * @author MJ
 */
public class myUtil {

    public static void setFocus(JTextField txtf) {

        SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                txtf.requestFocus();
            }
        });
    }

}
