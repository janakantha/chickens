/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import classes.ConManager;
import java.awt.Font;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author MJ
 */
public class reports_panel extends javax.swing.JPanel {

    /**
     * Creates new form reports_panel
     */
    public reports_panel() {
        initComponents();

        loadMonthlyData();
        loadcqTable();
        jTable6.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
        jTable1.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 12));
        jMonthChooser1.setVisible(false);
        jButton5.setVisible(false);

    }

    private void loadcqTable() {
        try {
            DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();
            ResultSet rs = ConManager.getData("select * from cheque order by exp_date desc");
            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getString("cheque_no"));
                v.add(rs.getString("cheque_type"));
                v.add(rs.getString("cheque_amount"));
                v.add(rs.getString("cheque_date"));
                v.add(rs.getString("exp_date"));

                dtm.addRow(v);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadMonthlyData() {

        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DAY_OF_MONTH, 1);
        int myMonth = cal.get(Calendar.MONTH);
        DefaultTableModel dtm = (DefaultTableModel) jTable6.getModel();
        dtm.setRowCount(0);
        //String Today = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        while (myMonth == cal.get(Calendar.MONTH)) {

            String day = new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime());
            String dayEnd = day + " 23:59:59";
            if (new Date().compareTo(cal.getTime()) < 0) {
                return;
            }
            //
            String tot_day_purc = "0.00";
            String tot_day_paid = "0.00";
            String tot_day_sale = "0.00";
            String tot_day_expence = "0.00";

            try {
                ResultSet today_customers = ConManager.getData("select sum(pay_amount) as 'tot_day_paid' from cash_rec "
                        + "where cash_rec.date between '" + day + "' and '" + dayEnd + "'");

                if (today_customers.next()) {

                    tot_day_paid = today_customers.getString("tot_day_paid");

                    if (tot_day_paid == null) {
                        tot_day_paid = "0.00";
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
                return;

            }
            try {
                ResultSet inv_data = ConManager.getData("select sum(invoice_tot) as 'tot_day_sale' from invoice "
                        + "where date between '" + day + "' and '" + dayEnd + "'");
                if (inv_data.next()) {
                    tot_day_sale = inv_data.getString("tot_day_sale");
                    if (tot_day_sale == null) {
                        tot_day_sale = "0.00";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            try {
                ResultSet inv_data = ConManager.getData("select sum(sub_total) as 'tot_day_purc' from grn join grn_detail "
                        + "on grn.idgrn = grn_detail.grn_idgrn "
                        + "where grn.date between '" + day + "' and '" + dayEnd + "'");

                if (inv_data.next()) {
                    tot_day_purc = inv_data.getString("tot_day_purc");
                    if (tot_day_purc == null) {
                        tot_day_purc = "0.00";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            try {
                ResultSet inv_data = ConManager.getData("select sum(amount) as 'tot_day_expence' from expence "
                        + "where date between '" + day + "' and '" + dayEnd + "'");
                if (inv_data.next()) {
                    tot_day_expence = inv_data.getString("tot_day_expence");
                    if (tot_day_expence == null) {
                        tot_day_expence = "0.00";
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }

            Vector<String> v = new Vector();

            v.add(day);
            v.add(tot_day_purc);
            v.add(tot_day_sale);
            v.add(tot_day_paid);
            v.add(tot_day_expence);

            dtm.addRow(v);

            cal.add(Calendar.DAY_OF_MONTH, 1);//next day
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        jButton5 = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        jTable6 = new javax.swing.JTable();
        jMonthChooser1 = new com.toedter.calendar.JMonthChooser();
        jButton6 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jButton5.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jButton5.setText("Find");

        jTable6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Date", "Total Daily Purchased", "Total Daily Amount", "Total Daily Paid", "Total Daily Expences"
            }
        ));
        jScrollPane6.setViewportView(jTable6);

        jButton6.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jButton6.setText("Get Daily Report");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(374, 374, 374)
                        .addComponent(jMonthChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 843, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(128, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jMonthChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 491, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Monthly Sales", jPanel4);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cheaque Number", "Cheaque Type", "Amount", "Cheaque In date", "Expaire date"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(88, 88, 88)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 788, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(156, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(109, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Cheques Details", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 605, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        int selectedRow = jTable6.getSelectedRow();

        if (jTable6.isRowSelected(selectedRow)) {
            String sel_date = jTable6.getValueAt(selectedRow, 0).toString();
            String full_path = classes.reports.daily_Sales(sel_date);

            if (full_path != null) {
                System.out.println(full_path);
                int reply = JOptionPane.showConfirmDialog(null, "Do You want Print this Document ?", "Daily Report Genarated Successfully !", JOptionPane.YES_NO_OPTION);
                if (reply == JOptionPane.YES_OPTION) {

                    classes.Printer.printDocument(full_path);

                } else {

                }

            }

        }
    }//GEN-LAST:event_jButton6ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private com.toedter.calendar.JMonthChooser jMonthChooser1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTable jTable6;
    // End of variables declaration//GEN-END:variables
}
